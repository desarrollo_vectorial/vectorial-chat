'use strict';

module.exports = function(db) {

	return {

		list_chats : function(sender_id, receiver_id, callback) {
			//db.query('SELECT * FROM engine4_chat_chats WHERE (sender_id = ' + sender_id + ' AND receiver_id = ' + receiver_id + ') OR (receiver_id = ' + sender_id + ' AND sender_id = ' + receiver_id + ') ORDER BY created', function(err, rows, fields) {
			db.query('SELECT * FROM engine4_chat_chats WHERE (sender_id = ? AND receiver_id = ?) OR (receiver_id = ? AND sender_id = ?) ORDER BY id ASC', [sender_id, receiver_id, sender_id, receiver_id], function(err, rows, fields) {
				if (!err)
					callback(rows);
				else
					console.log('Error Query.');
			});
		},

		save_chat : function(sender_id, receiver_id, message) {
			//var date = new Date().toISOString().substring(0, 19).replace('T', ' ');
			//nDate = new Date().toLocaleString('es-ES', { timeZone: 'America/Bogota' });
			//new Date().toLocaleString('en-GB', { year: 'numeric', month : 'numeric', day : 'numeric' })
			var date = new Date().toLocaleString('sv-SE', { timeZone: 'America/Bogota' }).toString();

			db.query("INSERT INTO engine4_chat_chats(sender_id, receiver_id, message, created) VALUES ?", [[[sender_id, receiver_id, message, date]]], function(err, result) {
				if (err)
					console.log('Error Query.');
			});
		}
		
	};

};