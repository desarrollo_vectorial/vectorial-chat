var usersOnline = {};

exports.socket = function socket(socket, io, chat_model) {
	//console.log(socket.id);
	//nueva conexión
	data_conn = socket.handshake.query;
	var userdata = {
		socket_id : socket.id,
		user_id : data_conn.user_id,
		name : data_conn.user_name.toString().trim(),
		email : data_conn.user_email,
		image : data_conn.user_image
	};
	//se adiciona al usuario a la lista
	usersOnline[data_conn.user_id] = userdata;
	socket.userdata = userdata;

	// se actualiza la lista de usuarios en el chat del lado del cliente
	io.emit('user_list', {
		users : usersOnline
	});

	// envia el msg al usuario
	socket.on('message', function(msg) {
		//console.log('message from: ' + socket.id); console.info(msg);
		user_to = usersOnline[msg.user_id];

		if (user_to !== undefined && user_to.socket_id !== undefined && io.sockets.connected[user_to.socket_id] !== undefined) {
			data = {
				msg : msg.msg,
				user_id : socket.userdata.user_id
			};
			io.sockets.connected[user_to.socket_id].emit('new_message', data);
			//socket.broadcast.to(user_to.socket_id).emit('send:messageprivate',{});
			// se guarda el registro
			chat_model.save_chat(socket.userdata.user_id, user_to.user_id, msg.msg);
			//io.emit('new_message', msg);
		} else {
			socket.emit('error_msg', {
				user_id : msg.user_id
			});
		}
	});

	socket.on('getall_messages', function(data) {
		//io.emit('chat message', msg);
		user_to = usersOnline[data.sender_id];

		chat_model.list_chats(data.sender_id, data.receiver_id, function(result) {
			data = {
				messages : result,
				receiver_id : data.receiver_id
			};

			if (user_to !== undefined) {
				io.sockets.connected[user_to.socket_id].emit('send_all_messages', data);
			}
		});
	});

	socket.on('disconnect', function() {
		//console.log('user ' + socket.id + ' disconnected');
		if (socket.userdata != null) {
			delete usersOnline[socket.userdata.user_id];
			// se actualiza la lista de usuarios en el chat del lado del cliente
			io.emit('user_list', {
				users : usersOnline
			});
			//socket.broadcast.emit('send:messageothersusers', {}); - io.sockets.emit("updateSidebarUsers", usuariosOnline);
		}
	});
	
};

//module.exports.socket = socket;