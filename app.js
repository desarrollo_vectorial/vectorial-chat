'use strict';

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./db');
var chat_model = require('./models/chat')(db);
var _socket = require('./socket');

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Authorization, Content-Length, X-Requested-With, Accept');
	// res.setHeader('Access-Control-Allow-Credentials', true);
	next();
});

app.get('/', function(req, res) {
	res.send('<h1>CHAT</h1>');
	//res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
	_socket.socket(socket, io, chat_model);
});

http.listen(3005, function() {
	console.log('listening on *:3005');
	//console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});