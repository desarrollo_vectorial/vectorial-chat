//npm init (inicializar proyecto)

npm install (instalar dependencias)

iniciar el servicio
NODE_ENV=dev node app.js
pm2 start config.json 
pm2 start config.json --env prod

// pruebas
export NODE_ENV=test
pm2 start app.js

//detener
pm2 list -> retorna el 'name_app'
pm2 stop 'name_app'

//reiniciar
pm2 restart 'name_app'

// instalar pm2 para correr el servicio
sudo npm install pm2 -g

// otros
pm2 startup => restart
pm2 monitor

// extra
console.log(process.argv); -> node app.js token=my-token env=prueba

NODE_ENV=production forever start ./bin/node.js

NODE_ENV=redis node app.js
if (process.env.NODE_ENV == 'production') db = require('mongoskin').db('localhost:37751/bands');

// apache conf
<VirtualHost *:80>
    ServerName chat.vectorial.co
    ServerSignature Off
    ProxyRequests Off
    <Proxy *>
     	Order Allow,Deny
      Allow from all
    </Proxy>
    ProxyPass / http://127.0.0.1:3005/
    ProxyPassReverse / http://127.0.0.1:3005/
    ProxyVia On
</VirtualHost>


<VirtualHost *:80>
     ServerName chat.vectorial.co
     ServerAlias chat.vectorial.co

     ProxyRequests Off

     ErrorDocument 503 /503.html
     ProxyPass /503.html !

     ProxyPass / http://localhost:3005/
     ProxyPassReverse / http://localhost:3005/

		RewriteEngine on
		RewriteCond %{HTTP:UPGRADE} ^WebSocket$ [NC]
		RewriteCond %{HTTP:CONNECTION} ^Upgrade$ [NC]
		RewriteRule .* ws://localhost:3005%{REQUEST_URI} [P]
</VirtualHost>





<VirtualHost *:80>
    ServerName chat.vectorial.co
    <Proxy *>
     	Order Allow,Deny
      Allow from all
    </Proxy>
    ProxyRequests off
    ProxyPass /socket.io/ ws://localhost:3005/socket.io/
    ProxyPassReverse /socket.io/ ws://localhost:3005/socket.io/
</VirtualHost>

RewriteEngine On
RewriteCond %{REQUEST_URI}  ^/socket.io/1/websocket  [NC]
RewriteRule /(.*)           ws://localhost:3005/$1 [P,L]

ProxyPass        /socket.io http://localhost:3005/socket.io
ProxyPassReverse /socket.io http://localhost:3005/socket.io





// nginx
server {
    listen 80;
    root /home/chl/chat;
    index index.html index.htm;
 
    server_name chat.vectorial.co;
 
    location / {
        proxy_pass http://localhost:3005;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host $host;
    }
}

**
http://pm2.keymetrics.io/docs/usage/pm2-doc-single-page/
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-node-js-application-for-production-on-ubuntu-14-04
*
https://forums.meteor.com/t/how-do-you-fix-websocket-error/2827/10
