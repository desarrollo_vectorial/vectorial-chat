'use strict';

var mysql = require('mysql');

if (process.env.NODE_ENV == 'test') {
	var host = 'localhost';
	var user = 'root';
	var password = 'vectorial$_$';
	var database = "incauca_intranet";
} else if (process.env.NODE_ENV == 'prod') {
    var host = '127.0.0.1';
    var user = 'agrln';
    var password = 'rbyWdrj0KxNwJjM';
    var database = "agrln";
} else { // dev
	var host = 'localhost';
	var user = 'root';
	var password = 'root';
	var database = "dev_incauca_intranet";
}

var pool = mysql.createPool({
	connectionLimit : 10,
	host : host,
	user : user,
	password : password,
	database : database,
});

module.exports = pool;
//connection.end();